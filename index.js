const http = require("http");
const port = 4000;

const server = http.createServer((req, res) => {
    if (req.url === "/") {
        res.writeHead(200, { "Content-Type": "text/plain" });
        res.end("Welcome to Booking System!");
    } else if (req.url === "/profile") {
        res.writeHead(200, { "Content-Type": "text/plain" });
        res.end("Welcome to my profile");
    } else if (req.url === "/courses" && req.method === "GET") {
        res.writeHead(200, { "Content-Type": "text/plain" });
        res.end("Here are our available courses!");
    } else if (req.url === "/courses" && req.method === "POST") {
        res.writeHead(200, { "Content-Type": "text/plain" });
        res.end("Add a course to our resources");
    } else if (req.url === "/updatecourse" && req.method === "PUT") {
        res.writeHead(200, { "Content-Type": "text/plain" });
        res.end("Update a course to our resources");
    } else if (req.url === "/archivecourses" && req.method === "DELETE") {
        res.writeHead(200, { "Content-Type": "text/plain" });
        res.end("Archive courses to our resources");
    } else {
        res.end("Page not Found");
    }
});

server.listen(port, () => {
    console.log(`Listening to port ${port}`);
});
